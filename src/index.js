import Botkit from 'botkit';
import gitlab from './gitlab';
import codeship from './codeship';

import {SLACK_ID, SLACK_SECRET} from './const';

var bot_options = {
  clientId: SLACK_ID,
  clientSecret: SLACK_SECRET,
  debug: true,
  scopes: ['bot'],
  json_file_store: './db_slackbutton_bot/',
};

const controller = Botkit.slackbot(bot_options);

controller.startTicking();

// set up a botkit app to expose oauth and webhook endpoints
controller.setupWebserver(process.env.port, () => {
  controller.createWebhookEndpoints(controller.webserver);

  controller.createOauthEndpoints(controller.webserver, (err,req,res) => {
    if (err) {
      res.status(500).send('ERROR: ' + err);
    } else {
      res.send('Success!');
    }
  });
});

controller.on('slash_command', (bot, message) => {
  let command = message.command;

  switch (command) {
  case '/create':
    return createDialog(bot, message);
  default:
    return;
  }

});

function createDialog(bot, message) {
  var dialog = bot.createDialog('Create New Project','callback_id','Create')
    .addText('Project Name','projectName','')
    .addSelect('Project Type','projectType',null,[{label:'Pro',value:'pro'},{label:'Basic',value:'basic'}],{placeholder: 'Select One'});
  bot.replyPrivate(message, 'Ok, let\'s create a new project');
  bot.replyWithDialog(message, dialog.asObject());
}

controller.on('dialog_submission', (bot, message) => {
  const projectType = message.submission.projectType;
  const projectName = message.submission.projectName;

  bot.dialogOk();
  bot.createPrivateConversation(message, (err,dm) => {
    dm.say(`I am now creating the Codeship ${projectType} project and \`${projectName}\` repo.`);
    gitlab.createRepo(projectName)
      .then(gitlab_res => {
        dm.say(`The GitLab Url is ${gitlab_res.web_url}`);
        return codeship.createProject(gitlab_res.ssh_url_to_repo, projectType);
      })
      .then(codeship_res => {
        dm.say(`The Codeship Project Url is https://app.codeship.com/projects/${codeship_res.project.id}`);
        dm.activate();
      })
      .catch(err => {
        // respond with error messages here
        console.log(err);
      });
  });

});

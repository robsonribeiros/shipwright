import fetch from 'node-fetch';
import {GITLAB_TOKEN, GITLAB_BASE_URL} from './const.js';

const createRepo = function (projectName) {
  const options = {
    method: 'POST',
    headers: {
      'Private-Token': GITLAB_TOKEN,
      'Content-type': 'application/json',
    },
    body: JSON.stringify({
      name: projectName
    }),
  };

  return fetch(`${GITLAB_BASE_URL}/projects`, options)
    .then(response => Promise.all([response.ok, response.json()]))
    .then(([responseOk, body]) => {
      if (!responseOk) {
        throw new Error(body.message.name);
      } else {
        return body;
      }
    });
};

export default {
  createRepo
};

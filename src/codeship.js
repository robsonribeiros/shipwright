import fetch from 'node-fetch';
import {CODESHIP_BASE_URL, CODESHIP_TOKEN, CODESHIP_ORG_NAME, BASIC_TEMPLATE, PRO_TEMPLATE} from './const.js';

function auth() {
  const options = {
    method: 'POST',
    headers: {
      Authorization: `Basic ${CODESHIP_TOKEN}`,
    },
  };

  return fetch(`${CODESHIP_BASE_URL}/auth`, options)
    .then(res => res.json());
}

function getOrgId(orgs) {
  const result = orgs.find((org) => org.name === CODESHIP_ORG_NAME);
  return result.uuid;
}

const createProject = function (repositoryUrl, projectType) {
  let tmpl;

  if (projectType === 'basic') {
    tmpl = BASIC_TEMPLATE;
  } else {
    tmpl = PRO_TEMPLATE;
  }

  tmpl.repository_url = repositoryUrl;

  return auth().then(data => {
    const options = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${data.access_token}`,
        'Content-type': 'application/json',
      },
      body: JSON.stringify(tmpl),
    };

    return fetch(`${CODESHIP_BASE_URL}/organizations/${getOrgId(data.organizations)}/projects`, options)
      .then(res => res.json());
  });
};


export default {
  createProject
};

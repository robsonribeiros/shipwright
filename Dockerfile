FROM node:9.2.0-alpine

RUN mkdir /usr/app
WORKDIR /usr/app

RUN npm install -g nodemon

COPY package.json .
RUN npm install --quiet

COPY . .

CMD ["npm", "run", "dev"]
